using Microsoft.VisualStudio.TestTools.UnitTesting;
using ThousandNumberAddition;
using Xunit;

namespace AddingThousandNumbers
{
    [TestClass]
    public class UnitTest1
    {
        [Theory]
        [InlineData("", "Invalid")]
        [InlineData("1234", "Valid")]
        public void ValidNumber(string number, string expected)
        {
            NumberChecker numberChecker = new NumberChecker();
            string actual = numberChecker.CheckValidNumber(number);
            Xunit.Assert.Equal(expected, actual);
        }
    }
}
