using System;
using ThousandNumberAddition;
using Xunit;

namespace SumOfThousandNumbers
{
    public class SumThousandNumber
    {
        [Theory]
        [InlineData(" ","Valid")]
        [InlineData("1234a", "Invalid")]
        [InlineData("2434a", "Valid")]
        [InlineData("23412", "Invalid")]
        public void SummingThousandNumbers(string number, string expected)
        {
            NumberChecker numberChecker = new NumberChecker();
            string actual = numberChecker.CheckValidNumber(number);
            Assert.Equal(actual,expected);
        }
    }
}
