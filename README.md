# AddingThousandDigitNumberWithTest
This project is all about adding two thousand number of digit which is out of range of our standard data-types.
So, the addition should be done in alternative way by taking them as string, manipulating them as string 
and showing output as string. As for test purposes, I have used unit test for checking the numbers validity.

## Getting started
I have pure C# .NET for this purpose, xUnit for testing and Visual studio as an code editor

## Procedures
There is no ending with the improvement. 

So far, I have done the project as below-mentioned procedure:<br>
-> Taking two numbers as string<br>
-> Checking and making the same length for the taken strings by padding to left <br>
-> Convert to charArray and convert to int individually<br>
-> As the length of two numbers are same, I started a for loop according to the length of the string<br>
-> My initial calculation is sum will be done by adding two int values and the carry if it exists<br>
-> Then followed by conditional statements<br>
-> First one checking sum is greater than 9 or not and its the last digit<br>
-> Second one checking only sum is greater than 9 or not<br>
-> Third one returns the carry/remainder value to 0 if sum is less than or equal to 9<br>
-> After doing that, time to print the sum as string<br>
-> As I have added the digits as strings, I reveresed the string to get the desired output (total number)<br>
-> I have used Unit testing to check whether the number is valid or not

## Contributing
I do really appreciate those people who are open-minded and come with better suggestions to improve the project

## Authors and acknowledgment
As a author, I have taken lot of inspiration from Mirza who helps a lot with his insights

## License
There is no license available for this project
