﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThousandNumberAddition
{
    public class NumberChecker
    {
        public string CheckValidNumber(string number)
        {
            int count = 0;

            char[] num = number.ToCharArray();   
            
            char[] ch = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            //char[] no = number.ToCharArray();
            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < ch.Length; j++)
                {
                    if(num[i]==ch[j])
                        count++;
                }
                
            }
            if (count == number.Length)
                return "Valid";
            else
                return "Invalid";
        }
    }
}
