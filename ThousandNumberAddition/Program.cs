﻿using System;
using System.Text;

namespace ThousandNumberAddition
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberChecker numberChecker = new NumberChecker();
            numberChecker.CheckValidNumber("2434a");
            Console.Write("Enter your first number: ");
            string number1 = Console.ReadLine();
            Console.Write("Enter your second number: ");
            string number2 = Console.ReadLine();

            string sumOfNumbers = AddNumbers(number1, number2);
            Console.WriteLine("Sum of numbers: "+sumOfNumbers);

            Console.ReadLine();
        }

        public static string AddNumbers(string number1, string number2)
        {

            if (number1.Length < number2.Length)

                number1 = number1.PadLeft(number1.Length + (number2.Length - number1.Length), '0');

            if (number1.Length > number2.Length)

                number2 = number2.PadLeft(number2.Length + (number1.Length - number2.Length), '0');

            char[] ch1 = number1.ToCharArray();
            char[] ch2 = number2.ToCharArray();
            string total = "";
            int remainder = 0;

            for (int i = (number1.Length-1); i >= 0; i--)
            {
                int convertedCharactersOf1 = Convert.ToInt32(new string(ch1[i], 1));
                int convertedCharactersOf2 = Convert.ToInt32(new string(ch2[i], 1));
                int sum = convertedCharactersOf1 + convertedCharactersOf2 + remainder;

                if (sum > 9 && i == 0)
                {
                    remainder = (sum / 10);
                    sum = sum % 10;
                    total = total + remainder.ToString() + sum.ToString();
                    //break;
                }

                else if (sum > 9)
                {
                    remainder = (sum / 10);
                    sum = sum % 10;
                }
                else
                    remainder = 0;
                total = total + sum.ToString();
            }

            char[] reverse = total.ToCharArray();
            string finalOutput = String.Empty;
            for (int i = total.Length - 1; i >= 0; i--)
            {
                finalOutput += reverse[i];
            }

            return finalOutput;
        }
    }
}
